function Template = Gaussian_Template(X,Y,Sigma);

Total=0;%used to normalise the Gaussian Template

%since we always want there to be a centre point it is important that the dimensions
%of the template is odd x odd therefore if one of the values is even
%this will automatically taken to the next odd integer.
if rem(X,2) == 0
X=X+1;
end
if rem(Y,2) == 0
Y=Y+1;
end
%finding the center point of [x,y], since this can be a uneven
%kernel size both the x-axis and the y-axis centre needs to be found.
Centre_Point_X=floor(X/2)+1;
Centre_Point_Y=floor(Y/2)+1;

for a=1:Y%implementing the Gaussian Template for the Low Pass Filtered Image
 for b=1:X 
 Gaussian_Template(b,a)=exp(-(((b-Centre_Point_X)^2)+((a-Centre_Point_Y)^2))/(2*Sigma^2));
 Total=Total+Gaussian_Template(b,a);
 end
end
Template=Gaussian_Template/Total;
%Normalising the Gaussian Template