function Sample = Down_Sampling(Hybrid_Image)
%In this function we are downsampling the image by 0.5 for 4 times to get a
%new image created by 5 different sized hybrid images to be able to
%interpret the hybrid image better.
scale_size = 5; 
padding = 5;
Sample = Hybrid_Image;
Sample_1 = Hybrid_Image;
for i = 2:scale_size
    Sample_1 = Sample_1(1:2:end,1:2:end,:);
    Cur_Img = cat(1,ones(size(Hybrid_Image,1) - size(Sample_1,1), size(Sample_1,2), size(Hybrid_Image,3)), Sample_1);
    Sample = cat(2, Sample, Cur_Img); %the current downsample of the image is being added to the previous sample.
end