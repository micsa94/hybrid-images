function Filter_Image = fast_fourier(image, filter);
%in this function the 2d fast fourier transform is being used to do the
%convolution of the image and the filter
[image_row,image_col] = size(image(:,:,1));
[filter_row, filter_col] = size(filter);
%to implement the convolution of a fft the image and the filter need to be
%of same length, therefore the new value for the rows and columns in being
%found by adding both together minus 1
row = image_row+filter_row - 1;
col = image_col+filter_col - 1;

if size(image,3)==3%used for RGB images
    %using the fft2 each layer is being convolved with the filter
    R = (fft2(image(:,:,1),row,col).* fft2(filter,row,col));
    G = (fft2(image(:,:,2),row,col).* fft2(filter,row,col));
    B = (fft2(image(:,:,3),row,col).* fft2(filter,row,col));
    %%the three layers are being added and the inverse FFT is being done 
    Filter_Image = ifft2(cat(3,R,G,B));
    [flt_img_row, flt_img_col]=size(Filter_Image(:,:,1));
    diff_in_row = (flt_img_row - image_row)/2;
    diff_in_col = (flt_img_col - image_col)/2;
    Filter_Image = Filter_Image(diff_in_row+1:end-diff_in_row,diff_in_col+1:end-diff_in_col,:);
    %since the filters need to be subtracted or added further in the main
    %unpadding is taking place so that the filtered image and image would
    %have the same size.
end
if size(image,3)==1%used for grayscale images
    Filter_Image = ifft2(fft2(image,row,col).* fft2(filter,row,col));
    [flt_img_row, flt_img_col]=size(Filter_Image(:,:));
    diff_in_row = (flt_img_row - image_row)/2;
    diff_in_col = (flt_img_col - image_col)/2;
    Filter_Image = Filter_Image(diff_in_row+1:end-diff_in_row,diff_in_col+1:end-diff_in_col);
end