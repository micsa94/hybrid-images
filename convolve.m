function Filtered_Image = convolve(image, filter);
%this function is calling another function to do the convolution

%if image is rgb then each color is being separated calling the convolution
%function and added together to create the filtered image
if size(image,3)==3
    R=image(:,:,1);
    G=image(:,:,2);
    B=image(:,:,3);
    R_new=Convolution(R,filter);
    G_new=Convolution(G,filter);
    B_new=Convolution(B,filter);
    Filtered_Image = cat(3,R_new,G_new, B_new);
end

%if image is grayscale then the colvolution function can immediately take
%place without having to separate the layers of the image.
if size(image,3)==1
    Filtered_Image = Convolution(image,filter);
end