function [Img1, Img2] = Image_Option()

%This function is being used so that the user can choose which images are
%going to be combined to create the hybrid image
%There are a total of 5 options that can by used in this program.
%The user must enter a number between 1 and 5 so that the program will
%continue. If another integer is inserted in the command window the user
%will be asked to re-enter a number in that region.
%Since it only allows for integers if any character is entered an error
%will be shown and the user will be asked to enter the number again.
addpath('C:/Users/micsii/Desktop/Southampton/Computer Vision/Coursework_1/Coding/data');

disp('1. Cat and Dog')
disp('2. Bird and Plane')
disp('3. Fish and Submarine')
disp('4. Bike and Motorcycle')
disp('5. Marilyn and Einstein')
disp('6. Happy Face and Neutral Face')
number = 'Please choose the number of the images you would like to combine ';
hybrid_option = input(number);

if (hybrid_option == 1)
    Img1 = imread('dog.bmp');
    Img2 = imread('cat.bmp');

elseif (hybrid_option == 2);
    Img1 = imread('bird.bmp');
    Img2 = imread('plane.bmp');

elseif (hybrid_option == 3);
    Img1 = imread('fish.bmp');
    Img2 = imread('submarine.bmp');

elseif (hybrid_option == 4);
    Img1 = imread('bicycle.bmp');
    Img2 = imread('motorcycle.bmp');

elseif (hybrid_option == 5);
    Img1 = imread('marilyn.bmp');
    Img2 = imread('einstein.bmp');

elseif (hybrid_option == 6);
    Img1 = imread('normal.jpg');
    Img2 = imread('happy.jpg');
else
    disp('Please run again as it is an invalid number')
    Img1=0;
    Img2=0;
end
