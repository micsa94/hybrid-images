clc;
close all;%close all figures
clear all;%clears all previous data

[Img1, Img2] = Image_Option();
if (Img1 == 0)
    [Img1, Img2] = Image_Option();%calling the image data according to preference chosen
end
Img_1=im2single(Img1);%convert image to single matrix
Img_2=im2single(Img2);
%Checking if greyscale convolution is supported
%Img_1 = rgb2gray(Img_1);%to check please remove commented sign (%)for both images
%Img_2 = rgb2gray(Img_2);

X=49;%defining the window size for the 2D Gaussian function
Y=5;

Gaussian_Template_1 = Gaussian_Template(X,Y,11);
Gaussian_Template_2 = Gaussian_Template(X,Y,5.5);
%The 2D Fast Fourier Transform will be used in the Convolution function to
%implement a 2D Gaussian Filter

Low_Pass_Filter_1 = convolve(convolve(Img_1,Gaussian_Template_1),Gaussian_Template_1');
figure ; imshow(Low_Pass_Filter_1);title('Low Pass Filter')%Low Pass Filtered Version of Image 1

[Low_Pass_Filter_2] = convolve(convolve(Img_2, Gaussian_Template_2),Gaussian_Template_2');
High_Pass_Filter = Img_2 - Low_Pass_Filter_2;
%meanIntensity = mean(High_Pass_Filter(:))%checking the zero mean of the high pass filter
%By reducing the Low Pass Filter from the original image we will determine the High Pass Filtered image
figure ; imshow(High_Pass_Filter+ 0.5);title('High Pass Filter')%High Pass Filtered Version of Image 2

Hybrid_Image = Low_Pass_Filter_1 + High_Pass_Filter;%combining the low pass and high pass filter to create the hybrid image
Scaled_Image = Down_Sampling(Hybrid_Image);
figure ; imshow(Scaled_Image);title('Scaled Hybrid Image')
