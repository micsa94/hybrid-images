function filtered_image = convolution(image, filter);
[filter_row filter_col] = size(filter);
[image_row image_col] = size(image);
%padding image by filter size - 1 since odd*odd kernel and divided by 2 so
%that have the size is added above and below, same for left and right
%pixels in the image
image_pad = padarray(image, [(filter_row - 1)/2, (filter_col - 1)/2]);
%set image as a column of size filter_row*filter_col
image_new = im2col(image_pad, [filter_row, filter_col]);
%The image is being resized by the filter size to be able to implement
%convolution
% transpose image to do convolution
filter_t = transpose(filter(:));
% filter the image
low_pass = filter_t * image_new;
% convert from column back to image to get the right output
filtered_image = col2im(low_pass, [1, 1], [image_row, image_col]);

